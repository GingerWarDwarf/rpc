/* rdbase_srp.c - initw, insertw, deletew, lookupw */

#include <rpc/rpc.h>
#include <stdio.h>
#include <string.h>

#include "rdbase.h"
char dict[DATABSIZ][MAXWORD + 1];
int nwords = 0;

int initw()
{
  printf("ha");
  nwords = 0;
  return 1;
}

int insertw(word) char* word;
{
  int i;
  for (i = 0; i < nwords; i++)
    if (strcmp(word, dict[i]) == 0)
      return 0;
  strcpy(dict[nwords], word);
  nwords++;
  return nwords;
}

int deletew(word) char* word;
{
  int i;
  for (i = 0; i < nwords; i++)
    if (strcmp(word, dict[i]) == 0) {
      nwords--;
      strcpy(dict[i], dict[nwords]);
      return 1;
    }
  return 0;
}

int lookupw(word) char* word;
{
  int i;
  for (i = 0; i < nwords; i++)
    if (strcmp(word, dict[i]) == 0)
      return 1;
  return 0;
}

int updatew(word, word2) char *word, *word2;
{
  int i;
  for (i = 0; i < nwords; i++)
    if (strcmp(word, dict[i]) == 0) {
      strcpy(dict[i], word2);
      return 1;
    }
  return 0;
}

int countw() { return nwords; }

oneword selectw(){
  char bla[DATABSIZ*(MAXWORD+1)];
  for(int i =0; i<sizeof(dict+1); i++){
    strcat(bla, dict[i]);
    strcat(bla," ");
  }
  oneword test;
  test.word=bla;
  return test;
}

manywords select2w(){
  manywords bleb;
  bleb.words.words_val = malloc(sizeof(struct oneword) * DATABSIZ);
  bleb.words.words_len = nwords;
  for(int i =0; i<nwords; i++){
    bleb.words.words_val[i].word = dict[i];
  }
  return bleb;


}

